# README #

Short mod to add rock bins to Dwarven civs. PRs welcome if this breaks.

* v4 -- 05/06/2020 -- Updated to 47.x compatibility
* v3 -- 11/29/2017 -- Updated to 44.x compatibility
* v2 -- 2/07/2017 -- Added category to reaction.
* v1 -- Initial release.

### Compatibility ###

* DF 47.x